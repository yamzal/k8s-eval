# Evaluation kubernetes

## Deploy back end :

```
sh create.sh
```

## View running services pods and deployments

```
sh view.sh
```

---

# Bonus

In App.js replace

```
const URL = 'http://localhost:5400'
```

by

```
const URL = process.env.REACT_APP_BACKEND_URL || 'http://37.59.31.35' // IP du service
```

## Build Dockerfile :

```
docker build -t yamzal1/redis-react .
```

(Must be placed at the root of the react project)

## Push it to dockerhub :

```
docker push yamzal1/redis-react
```

## Deploy react front end :

```
sh front/create.sh
```

## Clean up :

```
sh front/delete.sh
sh delete.sh
```
