#!/bin/bash

echo "Services :"
kubectl get services | grep yamzal

echo "Deployments :"
kubectl get deployments | grep yamzal

echo "Pods :"
kubectl get pods | grep yamzal
