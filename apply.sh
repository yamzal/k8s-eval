#!/bin/bash

kubectl apply -f yamzal-redis-deployment.yaml
kubectl apply -f yamzal-node-redis-deployment.yaml
kubectl apply -f yamzal-node-redis-service.yaml
kubectl apply -f yamzal-redis-service.yaml
