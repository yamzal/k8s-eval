#!/bin/bash

kubectl delete -f yamzal-node-redis-service.yaml
kubectl delete -f yamzal-redis-service.yaml
kubectl delete -f yamzal-node-redis-deployment.yaml
kubectl delete -f yamzal-redis-deployment.yaml
