#!/bin/bash

kubectl create -f yamzal-redis-deployment.yaml
kubectl create -f yamzal-node-redis-deployment.yaml
kubectl create -f yamzal-node-redis-service.yaml
kubectl create -f yamzal-redis-service.yaml
